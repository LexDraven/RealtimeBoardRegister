import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RegisterTest {
    private RegisterPage page;
    private Browser browser;
    private String userName = "testName";
    private String userEmail = "testmail@test.com";
    private String userPass = "123456789";
    private String spacesText = "        ";


    @BeforeClass
    public void start() {
        browser = new Browser(new ChromeDriver());
        page = new RegisterPage(browser);
    }

    @BeforeMethod
    public void methodStart() {
        browser.refresh();
    }

    @Test
    public void shouldTermsLinkBeAlive() {
        Assert.assertTrue(page.isTermsLinkActive());
    }

    @Test
    public void shouldIndexLinkBeAlive() {
        Assert.assertTrue(page.isIndexLinkActive());
    }

    @Test
    public void shouldLoginLinkBeAlive() {
        Assert.assertTrue(page.isLoginLinkActive());
    }

    @Test
    public void shouldAllElementsBeOnPage() {
        Assert.assertTrue(page.isAllElementsExists());
    }

    @Test
    public void shouldNotRegisterWithoutName() {
        page.register("",userEmail, userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Please enter your name");
    }

    @Test
    public void shouldNotRegisterWithoutEmail() {
        page.register(userName,"", userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Please enter your email address");
    }

    @Test
    public void shouldNotRegisterWithoutPassword() {
        page.register(userName,userEmail, "");
        Assert.assertTrue(page.waitForErrorMessageVisible());
    }

    @Test
    public void shouldNotRegisterWithSpacesInPassword() {
        page.register(userName,userEmail, spacesText);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Please enter your password");
    }

    @Test
    public void shouldNotRegisterWithShortPassword() {
        page.register(userName,userEmail, "123123");
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertTrue(page.isShortPasswordMessageExists());
    }

    @Test
    public void shouldNotRegisterWithSpacesInName() {
        page.register(spacesText,userEmail, userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Please enter your name");
    }

    @Test
    public void shouldNotRegisterWithSpacesInEmail() {
        page.register(userName,spacesText, userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Please enter your email address");
    }

    @Test
    public void shouldNotRegisterWithWrongEmail() {
        page.register(userName,browser.getRandomString(15), userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "This doesn’t look like an email address. Please check it for typos and try again.");
    }

    @Test
    public void shouldNotRegisterIfEmailIsRegistered() {
        page.register(userName,"lexman2@ya.ru", userPass);
        Assert.assertTrue(page.waitForErrorMessageVisible());
        Assert.assertEquals(page.getErrorMessage(), "Sorry, this email is already registered");
    }

    @Test (priority = 10)
    public void shouldRegisterIfAllFieldsNormal() {
        page.register(userName,browser.getRandomString(8)+"@test.com", userPass);
        Assert.assertTrue(browser.waitForVisibleElementWithText("Please check your email"));
        Assert.assertEquals(browser.currentUrl(), "https://realtimeboard.com/email-confirm/");
    }

    @AfterClass (alwaysRun = true)
    public void stop() {
        browser.quit();
    }
}
