import org.openqa.selenium.By;

public class RegisterPage {

    private final Browser browser;
    private final String SIGN_UP_URL = "https://realtimeboard.com/signup/";

    private final By nameInput = By.id("name");
    private final By emailInput = By.id("email");
    private final By passwordInput = By.id("password");
    private final By googleButton = By.id("kmq-google-button");
    private final By facebookButton = By.className("signup__btn--facebook");
    private final By submitButton = By.className("signup__submit");
    private final By errorMessage = By.className("signup__error-item");
    private final By termsLink = By.xpath("//a[.='Terms of Service']");
    private final By indexLink = By.xpath("//a[@title='RealtimeBoard']");
    private final By loginLink = By.xpath("//a[.='Log in']");


    public RegisterPage(Browser browser) {
        this.browser = browser;
        browser.goTo(SIGN_UP_URL);
    }

    public boolean isAllElementsExists(){
        return browser.isElementPresent(nameInput) && browser.isElementPresent(emailInput) && browser.isElementPresent(passwordInput)
                && browser.isElementPresent(googleButton) && browser.isElementPresent(facebookButton) && browser.isElementPresent(submitButton)
                && browser.isElementPresent(termsLink) && browser.isElementPresent(indexLink) && browser.isElementPresent(loginLink);
    }

    public void enterName(String name) {
        browser.typeTextToInput(nameInput, name);
    }

    public void enterEmail(String email) {
        browser.typeTextToInput(emailInput, email);
    }

    public void enterPass(String pass) {
        browser.typeTextToInput(passwordInput, pass);
    }

    public void clickGetStartedButton() {
        browser.click(submitButton);
    }

    public void register(String name, String email, String pass) {
        enterName(name);
        enterPass(pass);
        enterEmail(email);
        clickGetStartedButton();
    }

    public boolean isTermsLinkActive() {
        return browser.isLinkAlive(browser.getElementInnerLink(termsLink));
    }

    public boolean isIndexLinkActive() {
        return browser.isLinkAlive(browser.getElementInnerLink(indexLink));
    }

    public boolean isLoginLinkActive() {
        return browser.isLinkAlive(browser.getElementInnerLink(loginLink));
    }

    public boolean waitForErrorMessageVisible() {
        return browser.waitForVisible(errorMessage);
    }

    public String getErrorMessage() {
        return browser.isElementPresent(errorMessage) ? browser.getElementText(errorMessage) : "";
    }

    public boolean isShortPasswordMessageExists() {
        return browser.waitForVisibleElementWithText("Password must be 8+ characters");
    }
}
