import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Browser {
    private final WebDriver driver;
    private int timeOut;

    public Browser(WebDriver driver) {
        this(driver, 3);
    }

    public Browser(WebDriver driver, int timeOut) {
        this.driver = driver;
        this.timeOut = timeOut;
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    public boolean isElementPresent(By locator) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try {
            return driver.findElements(locator).size() > 0;
        } finally {
            driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.MILLISECONDS);
        }
    }

    public String getRandomString(int length) {
        length = length < 1 ? 1 : length;
        return new Random().ints(length, (int) 'a', (int) 'z' + 1).mapToObj(n -> String.valueOf((char) n)).collect(Collectors.joining());
    }

    public boolean waitForVisible(By locator, int timeOut) {
        try {
            new WebDriverWait(driver, timeOut).until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean waitForVisible(By locator) {
        return waitForVisible(locator,timeOut);
    }

    public boolean isLinkAlive(String url) {
        try {
            new UrlChecker().waitUntilAvailable(timeOut, TimeUnit.SECONDS, new URL(url));
            return true;
        } catch (UrlChecker.TimeoutException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public String getElementInnerLink(By locator) {
        return driver.findElement(locator).getAttribute("href");
    }

    public void goTo(String path) {
        driver.get(path);
    }

    public void typeTextToInput(By locator, String text) {
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(text);
    }

    public String getElementText(By locator) {
        return driver.findElement(locator).getText();
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public boolean waitForVisibleElementWithText(String text) {
        return waitForVisible(By.xpath("//*[.='" + text + "']"), timeOut);
    }

    public String currentUrl() {
        return driver.getCurrentUrl();
    }

    public void quit() {
        driver.quit();
    }
}
